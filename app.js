const express = require('express');
const nodemailer = require('nodemailer');
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
const cors = require('cors'); // Importez le middleware cors
require('dotenv').config();


const app = express();
const port = 3001;

// Middleware pour analyser le corps des requêtes JSON
app.use(express.json());
// Configuration du middleware cors avec allowedHeaders
app.use(cors({
    allowedHeaders: ['Content-Type']
}));

// Utilisateurs enregistrés (pour simplifier, stockés en mémoire)
const users = [
    { email: 'utilisateur@example.com', password: 'motdepasse', verified: false },
    { email: 'utilisateur2@example.com', password: 'motdepasse2', verified: false }
];

// Configuration du transporteur Nodemailer
const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: process.env.MAIL,
        pass: process.env.PASSWORD
    }
});

/**
 * @swagger
 * tags:
 *   name: Authentification
 *   description: Gestion de l'authentification des utilisateurs
 */

/**
 * @swagger
 * /login:
 *   post:
 *     summary: Authentification d'un utilisateur
 *     tags: [Authentification]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               email:
 *                 type: string
 *               password:
 *                 type: string
 *     responses:
 *       '200':
 *         description: Authentification réussie, e-mail de vérification envoyé
 *       '401':
 *         description: Identifiants incorrects
 *       '500':
 *         description: Erreur lors de l'envoi de l'e-mail de vérification
 */

app.post('/login', (req, res) => {
    const { email, password } = req.body;

    // Vérification des identifiants
    const user = users.find(u => u.email === email && u.password === password);

    if (!user) {
        return res.status(401).json({ error: 'Identifiants incorrects' });
    }

    // Génération du code de vérification
    const verificationCode = Math.floor(100000 + Math.random() * 900000);

    // Options de l'e-mail
    const mailOptions = {
        from: process.env.MAIL,
        to: email,
        subject: 'Code de vérification pour l\'authentification à deux facteurs',
        text: `Votre code de vérification est : ${verificationCode}`
    };

    // Envoi de l'e-mail
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            console.error(error);
            res.status(500).json({ error: 'Erreur lors de l\'envoi de l\'e-mail de vérification' });
        } else {
            console.log('E-mail de vérification envoyé: ' + info.response);
            // Stockage du code de vérification côté serveur pour la vérification ultérieure
            user.verificationCode = verificationCode;
            res.json({ message: 'Authentification réussie, e-mail de vérification envoyé' });
        }
    });
});

/**
 * @swagger
 * /verify-code:
 *   post:
 *     summary: Vérification du code de vérification pour l'authentification à deux facteurs
 *     tags: [Authentification]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               email:
 *                 type: string
 *               verificationCode:
 *                 type: string
 *     responses:
 *       '200':
 *         description: Authentification à deux facteurs réussie
 *       '401':
 *         description: Code de vérification incorrect
 *       '404':
 *         description: Utilisateur non trouvé
 */

app.post('/verify-code', (req, res) => {
    const { email, verificationCode } = req.body;

    // Recherche de l'utilisateur par e-mail
    const user = users.find(u => u.email === email);

    if (!user) {
        return res.status(404).json({ error: 'Utilisateur non trouvé' });
    }

    // Vérification du code de vérification
    if (user.verificationCode === Number(verificationCode)) {
        // Authentification à deux facteurs réussie
        user.verified = true;
        res.json({ message: 'Authentification à deux facteurs réussie' });
    } else {
        res.status(401).json({ error: 'Code de vérification incorrect' });
    }
});

// Configuration de Swagger
const options = {
    definition: {
        openapi: '3.0.0',
        info: {
            title: 'API d\'authentification',
            version: '1.0.0',
            description: 'API pour l\'authentification des utilisateurs'
        },
    },
    apis: [__dirname + '/app.js'],
};

const specs = swaggerJSDoc(options);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(specs, { explorer: true })); // Ajout de l'option explorer

// Redirection de la route racine vers la documentation Swagger
app.get('/', (req, res) => {
    res.redirect('/api-docs');
});

// Démarrage du serveur
app.listen(port, () => {
    console.log(`Serveur démarré sur http://localhost:${port}`);
});
