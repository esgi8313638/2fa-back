Pour lancer le projet côté back vous devez faire un "npm run serve" ou "nodemon app.js" ca lancera le projet sur le port :3001. 
Ensuite vous vous rendez sur l'URL suivante: http://localhost:3001/api-docs/ pour voir la doc API.

Vous devez ensuite créer un fichier ".env" et ajouter les lignes :
MAil:votre_email
PASSWORD:le_mot_de_passe_genere

Ensuite, allez dans votre compte Gmail et générez un mot de passe de l'application au lien suivant :
https://myaccount.google.com/apppasswords

Ajouter le nom de la l'application : 'nodemailer'
Google va vous générer un mot de passe que vous devrez rentrer dans la variable globale PASSWORD dans le fichier ".env".

Dans le fichier app.js vous trouverez un tableau avec des utilisateurs. 
Vous devez rentrez un user avec un email que vous connaissez pour que l'envoie du mail se fasse et que vous puissiez récupérer le code envoyé.

Tout en gardant le serveur backend en route... Lancez le projet front avec un "ng serve" ou "npm run dev" qui ouvre le projet sur le port :4200 normalement. 
Dans le fichier authentification.service.ts vous trouverez en ligne 9 (normalement) la baseUrl qui doit avoir le port identique au port du backend.